<?php

namespace Helium\FileManager;

use Illuminate\Support\Facades\Storage;

class FileManager
{
	public static function uploadBase64Image ($file, $file_location) {
		$sData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file));
		return Storage::disk('s3')->put($file_location, $sData);
	}
}